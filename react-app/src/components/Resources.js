import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';

function AlwaysOpenExample() {
  return (
    <Accordion defaultActiveKey={['0']} alwaysOpen>


      <Accordion.Item eventKey="0">
        <Accordion.Header>Most Popular Programming Languages to Learn in 2022</Accordion.Header>
        <Accordion.Body>
          Once upon a time, the world of computer programming was a mysterious and exclusive place. Only a select handful of people were considered computer programmers with cutting-edge coding skills. Today, many IT jobs require a solid grasp of the top programming languages, and yes, we mean more than one. 

          If your plans to advance your career or change careers completely requires you to master a programming language, you might wonder which one to learn. After all, it will take time and money to learn the language, so you want to make the right choice.

        <Button href="https://www.simplilearn.com/best-programming-languages-start-learning-today-article" target="_blank">Read more</Button></Accordion.Body>
        
      </Accordion.Item>


      <Accordion.Item eventKey="1">
        <Accordion.Header>Top 10 Reasons to Learn JavaScript</Accordion.Header>
        <Accordion.Body>
          There are two reasons why it’s sometimes hard to make a choice; either there are too few options to choose from, or there are too many. When it comes to programming languages, there is an embarrassment of riches, which in turn can cause mental gridlock. There’s Python, Java, JavaScript, C/CPP, PHP, Swift, C#, Ruby, Objective-C, and SQL, and that’s not even the full list! How do you choose?

          Now, bear in mind that as far as programmers go, there’s no such thing as knowing too many languages. A programmer with a grasp of many languages is a programmer who is highly marketable and very much in demand. Upskilling is always a smart way to go.

        <Button href="https://www.simplilearn.com/reasons-to-learn-javascript-article" target="_blank">Read more</Button>
        </Accordion.Body>
        
      </Accordion.Item>


      <Accordion.Item eventKey="2">
        <Accordion.Header>Introduction To Python Basics</Accordion.Header>
        <Accordion.Body>
          Python is one of the most popular programming languages available today. It is widely used in various sectors of business, such as programming, web development, machine learning, and data science. Given its widespread use, it’s not surprising that Python has surpassed Java as the top programming language.

          This Python tutorial covers all the basics of Python and some advanced topics of the Python language, such as variables, data types, arrays, functions, and OOPs concepts, all of which will help you prepare for a career as a professional Python programmer.
        
        <Button href="https://www.simplilearn.com/learn-the-basics-of-python-article" target="_blank">Read more</Button>
        </Accordion.Body>
        
      </Accordion.Item>


    </Accordion>
  );
}

export default AlwaysOpenExample;