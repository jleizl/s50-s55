/*
My s53 activity solution

import React from 'react';
import { Link } from 'react-router-dom';

export default function noPage() {


	return (
		<div>
        <h1>404 Error</h1>
        <h1>Page Not Found</h1>
   	</div>
		)
}
*/

import Banner from '../components/Banner'



export default function Error () {

	const data = {
		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return (

		<Banner data={data} />

		)

}


